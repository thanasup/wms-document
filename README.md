# Warehouse management system

This manual api walks you through to WMS API for Client

## Client API

> UAT Url <https://uat.namyongwex.com/wms-api>
>
> PROD Url <https://prod.namyongwex.com/wms-api>

* [Authentication](docs/authentication.md)
* [Client](docs/client.md)
  * [Client Address](docs/client-address.md)
* [Warehouse](docs/warehouse.md)
  * [Warehouse Location](docs/warehouse-location.md)
* [Product](docs/product.md)
  * [Product Unit Of Measure](docs/product-uom.md)
* [Product Category](docs/product-category.md)
* [Product Brand](docs/product-brand.md)
* [Transport Partner](docs/transport-partner.md)
* [Advance shipment notice](docs/asn.md)
* [Delivery Order](docs/delivery-order.md)
* [Goods Delivery](docs/goods-delivery.md)
* [Stock Inventory](docs/stock-inventory.md)
* [Regional](docs/regional.md)

> **Note:**
> * datetime format : `yyyy-MM-dd HH:mm:ss` and timezone is `Asia/Bangkok (GMT+7)`
## Authorization (How to API with Namyong WMS)

* **Type:** `Bearer Token`

To send request must be attach Bearer Token type authorization  with Header section. Token use is

1. [Authentication](docs/authentication.md) with username and password
2. Attach with to Header section of every request invoked, with authentication type is `Bearer`

> **For example**
> 1. Authentication wiht username and password 
>
> `JavaScript` example
> ```javascript 
> var myHeaders = new Headers();
> myHeaders.append("Content-Type", "application/json");
> 
> var raw = JSON.stringify({
>   "username": "username",
>   "password": "password"
> });
> 
> var requestOptions = {
>   method: 'POST',
>   headers: myHeaders,
>   body: raw,
>   redirect: 'follow'
> };
> 
> fetch("https://uat.namyongwex.com/wms-api/accounts/authenticate", requestOptions)
>   .then(response => response.text())
>   .then(result => console.log(result))
>   .catch(error => console.log('error', error));
> ```
> 
> 2.  Attach with to Header section of every request invoked, with authentication type is `Bearer`
>
>`JavaScript` example
>
> ```javascript
> var myHeaders = new Headers();
> myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImQ2MmU2N2FiLTM1M2QtNGRlYS04MWM4LWIwN2YwZmFmODg2YSIsInJvbGVJZCI6ImFiMjc2MThjLTI4YTUtNDQ1NC1hNzBlLWNjOTAxMTc3N2JkYiIsImtleUFjY291bnQiOiIiLCJjbGllbnRJZCI6IiIsIm5iZiI6MTYzNzAzODg1NCwiZXhwIjoxNjM3MDQyNDU0LCJpYXQiOjE2MzcwMzg4NTR9.7y08wyBebJGowH7QXIJU6xzP2ohqiIoG5VIqF-JD24o");
> 
> var raw = "";
> 
> var requestOptions = {
>   method: 'GET',
>   headers: myHeaders,
>   body: raw,
>   redirect: 'follow'
> };
> 
> fetch("https://uat.namyongwex.com/wms-api/warehouse/short-information", requestOptions)
>   .then(response => response.text())
>   .then(result => console.log(result))
>   .catch(error => console.log('error', error));
> ```

## Refresh token

The token will expire within 60 minutes. When attach the token with to header section of every request invoked, APIs will check to expire of the token. Therefore, before the token expires you need to send a request to refresh the token. By activating the path [/accounts/refresh-token](docs/authentication.md)

> **Note:**
> * If token needs to be refreshed, it is recommended to refresh it 60 minutes before the token expires.