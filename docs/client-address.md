# Client Address
[Back to readme](../README.md)

> **Reference:**
> * get `ClientId` from response of [Authenticate](authentication.md)
> * get `ShortAccountResponse` from [Account short response](account.md)

## Search client address
* **GET:** `/client-address/search`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt     | string          | - |
ClientId      | string($uuid)   | - |
PageNumber    | integer($int32) | - |
PageSize      | integer($int32) | - |
OrderBy       | string          | - |

### Responses
> Example Value
```json
[
  {
    "code": "string",
    "name": "string",
    "description": "string",
    "phone": "string",
    "tel": "string",
    "fax": "string",
    "email": "string",
    "address": "string",
    "countryCode": "string",
    "provinceCode": "string",
    "cityCode": "string",
    "areaCode": "string",
    "postalCode": "string",
    "isActive": true,
    "isDefault": true,
    "keyAccount": "string",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "userCreateBy": {
      "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "empCode": "string",
      "title": "string",
      "firstName": "string",
      "lastName": "string",
      "email": "string",
      "empFullName": "string"
    },
    "userUpdateBy": {
      "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "empCode": "string",
      "title": "string",
      "firstName": "string",
      "lastName": "string",
      "email": "string",
      "empFullName": "string"
    },
    "createBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "updateBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2021-10-27T06:51:14.184Z",
    "updated": "2021-10-27T06:51:14.184Z"
  }
]
```
> Schema

name | type | nullable 
----- | ----- | ----- |
code	        | string                 | true  |
name	        | string                 | true  |
description	  | string                 | true  |
phone	        | string                 | true  |
tel	          | string                 | true  |
fax	          | string                 | true  |
email	        | string                 | true  |
address	      | string                 | true  |
countryCode	  | string                 | true  |
provinceCode	| string                 | true  |
cityCode	    | string                 | true  |
areaCode	    | string                 | true  |
postalCode	  | string                 | true  |
isActive	    | boolean                | - |
isDefault	    | boolean                | - |   
keyAccount	  | string                 | true  |
id	          | string($uuid)          | - |
userCreateBy	| ShortAccountResponse   | - |
userUpdateBy	| ShortAccountResponse   | true  |
createBy	    | string($uuid)          | - |
updateBy	    | string($uuid)          | true  |
created	      | string($date-time)     | - |
updated	      | string($date-time)     | true  |

----------------------------

## Client Address infomation
* **GET:** `/client-address/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Responses
> Example Value
```json
[
  {
    "code": "string",
    "name": "string",
    "description": "string",
    "phone": "string",
    "tel": "string",
    "fax": "string",
    "email": "string",
    "address": "string",
    "countryCode": "string",
    "provinceCode": "string",
    "cityCode": "string",
    "areaCode": "string",
    "postalCode": "string",
    "isActive": true,
    "isDefault": true,
    "keyAccount": "string",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "userCreateBy": {
      "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "empCode": "string",
      "title": "string",
      "firstName": "string",
      "lastName": "string",
      "email": "string",
      "empFullName": "string"
    },
    "userUpdateBy": {
      "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "empCode": "string",
      "title": "string",
      "firstName": "string",
      "lastName": "string",
      "email": "string",
      "empFullName": "string"
    },
    "createBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "updateBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2021-10-27T06:51:14.184Z",
    "updated": "2021-10-27T06:51:14.184Z"
  }
]
```
> Schema

name | type | nullable 
----- | ----- | ----- |
code	        | string                 | true  |
name	        | string                 | true  |
description	  | string                 | true  |
phone	        | string                 | true  |
tel	          | string                 | true  |
fax	          | string                 | true  |
email	        | string                 | true  |
address	      | string                 | true  |
countryCode	  | string                 | true  |
provinceCode	| string                 | true  |
cityCode	    | string                 | true  |
areaCode	    | string                 | true  |
postalCode	  | string                 | true  |
isActive	    | boolean                | - |
isDefault	    | boolean                | - |   
keyAccount	  | string                 | true  |
id	          | string($uuid)          | - |
userCreateBy	| ShortAccountResponse   | - |
userUpdateBy	| ShortAccountResponse   | true  |
createBy	    | string($uuid)          | - |
updateBy	    | string($uuid)          | true  |
created	      | string($date-time)     | - |
updated	      | string($date-time)     | true  |

----------------------------
