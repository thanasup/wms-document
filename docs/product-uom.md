# Product Unit Of Measure
[Back to readme](../README.md)
> **Note:**
> * Use `fromUom` to reference product unit data to create order
> * Note that the `fromUom.id` should be replaced with `uomId`

## Product short infomation
* **GET:** `/product-uom/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt         | string          | false |
ProductId         | array[string]   | true |
### Responses
* **Code:** `200` _Success_
> Example Value
> ```json
> [
>   {
>     "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
>     "product": {
>       "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
>       "code": "string",
>       "name": "string",
>       "nameTh": "string",
>       "description": "string",
>       "barcode": "string",
>       "shelfLife": 0,
>       "length": 0,
>       "width": 0,
>       "height": 0,
>       "weight": 0,
>       "productType": 0
>     },
>     "fromUom": {
>       "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
>       "code": "string",
>       "name": "string",
>       "symbol": "string",
>       "noOfUnit": 0,
>       "precision": 0
>     },
>     "toUom": {
>       "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
>       "code": "string",
>       "name": "string",
>       "symbol": "string",
>       "noOfUnit": 0,
>       "precision": 0
>     },
>     "finalQty": 0
>   }
> ]
> ```
> Schema
> 
> name | type |
> ----- | ----- |
> id	      | string($uuid) |
> product	  | [ProductShortResponse](product.md)  |
> fromUom	  | [UomShortResponse](uom.md)  |
> toUom	    | [UomShortResponse](uom.md)  |
> finalQty	| integer($int32) |
> 
> --------------