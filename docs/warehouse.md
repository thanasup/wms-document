# Warehouse
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `warehouseId`

## Warehouse short information
* **GET:** `/warehouse/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt     | string          | false |
Code          | string          | false |
Name          | string          | false |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "name": "string",
    "defaultLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "stagingLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  }
]
```
> Schema

name | type |
----- | ----- |
id	               | string($uuid) |
code	             | string   |
name	             | string   |
defaultLocationId	 | string($uuid)  |
stagingLocationId	 | string($uuid)  |


----------------------------
