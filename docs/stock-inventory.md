# Stock Inventory
[Back to readme](../README.md)

## Stock inventory search
* **GET:** `/stock-inventory/search`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
ClientId      | array[string]   | true |
WarehouseId   | array[string]   | true |
ZoneId        | array[string]   | - |
LocationId    | array[string]   | - |
ProductId     | array[string]   | - |
Status        | array[string]   | - |
PageNumber    | integer($int32) | - |
PageSize      | integer($int32) | - |
OrderBy       | string          | - |

### Responses
> Example Value
```json
[
  {
    "inboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "grnNo": "string",
    "batchNo": "string",
    "lotNo": "string",
    "palletNo": "string",
    "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "zoneId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "status": "string",
    "onHandQty": 0,
    "stagingQty": 0,
    "reservedQty": 0,
    "pickingQty": 0,
    "packingQty": 0,
    "transferQty": 0,
    "unusableQty": 0,
    "availableQty": 0,
    "mfgDate": "2022-02-09T09:53:17.318Z",
    "expiryDate": "2022-02-09T09:53:17.318Z",
    "stockDate": "2022-02-09T09:53:17.318Z",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "client": {
      ... Client short response
    },
    "warehouse": {
      ... Warehouse short response
    },
    "zone": {
      ... Zone short response
    },
    "product": {
      ... Product short infomation
    },
    "uom": {
      ... UOM short infomation
    },
    "location": {
      ... Warehouse location short response
    },
    "product": {
      ... Product short response
    },
    "locationTag": 0,
    "expiryDays": 0
  }
]
```
> Schema

name | type |
----- | ----- |
inboundId   | 	string($uuid)
grnNo   | 	string
batchNo   | 	string
lotNo   | 	string
palletNo  | 	string
warehouseId   | 	string($uuid)
clientId  | 	string($uuid)
zoneId  | 	string($uuid)
locationId  | 	string($uuid)
productId   | 	string($uuid)
status  | 	string
onHandQty   | 	integer($int32)
stagingQty  | 	integer($int32)
reservedQty   | 	integer($int32)
pickingQty  | 	integer($int32)
packingQty  | 	integer($int32)
transferQty   | 	integer($int32)
unusableQty   | 	integer($int32)
availableQty  | 	integer($int32)
mfgDate   | 	string($date-time)
expiryDate  | 	string($date-time)
stockDate   | 	string($date-time)
expiryDays	|   integer($int32)
id  | 	string($uuid)
client  | 	[ClientShortResponse](client.md)
warehouse | 	[WarehouseShortResponse](warehouse.md)
zone  | 	[ZoneShortResponse](zone.md)
location  | 	[LocationShortResponse](warehouse-location.md)
product    |	[ProductShortResponse](product.md)
----------------------------

## Stock inventory detail
* **GET:** `/stock-inventory/detail`
* **Media type:** `application/json` Controls Accept header.

### Parameters `Query`
name | type | required 
----- | ----- | ----- |
ClientId      | array[string]   | true |
WarehouseId   | array[string]   | true |
ZoneId        | array[string]   | - |
LocationId    | array[string]   | - |
ProductId     | array[string]   | true |
PageNumber    | integer($int32) | - |
PageSize      | integer($int32) | - |
OrderBy       | string          | - |

### Responses
> Example Value
```json
[
  {
    "inboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "grnNo": "string",
    "batchNo": "string",
    "lotNo": "string",
    "palletNo": "string",
    "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "zoneId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "status": "string",
    "onHandQty": 0,
    "stagingQty": 0,
    "reservedQty": 0,
    "pickingQty": 0,
    "packingQty": 0,
    "transferQty": 0,
    "unusableQty": 0,
    "availableQty": 0,
    "mfgDate": "2022-02-09T09:53:17.318Z",
    "expiryDate": "2022-02-09T09:53:17.318Z",
    "stockDate": "2022-02-09T09:53:17.318Z",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "client": {
      ... Client short response
    },
    "warehouse": {
      ... Warehouse short response
    },
    "zone": {
      ... Zone short response
    },
    "product": {
      ... Product short infomation
    },
    "uom": {
      ... UOM short infomation
    },
    "location": {
      ... Warehouse location short response
    },
    "product": {
      ... Product short response
    },
    "locationTag": 0,
    "expiryDays": 0
  }
]
```
> Schema

name | type |
----- | ----- |
inboundId   | 	string($uuid)
grnNo   | 	string
batchNo   | 	string
lotNo   | 	string
palletNo  | 	string
warehouseId   | 	string($uuid)
clientId  | 	string($uuid)
zoneId  | 	string($uuid)
locationId  | 	string($uuid)
productId   | 	string($uuid)
status  | 	string
onHandQty   | 	integer($int32)
stagingQty  | 	integer($int32)
reservedQty   | 	integer($int32)
pickingQty  | 	integer($int32)
packingQty  | 	integer($int32)
transferQty   | 	integer($int32)
unusableQty   | 	integer($int32)
availableQty  | 	integer($int32)
mfgDate   | 	string($date-time)
expiryDate  | 	string($date-time)
stockDate   | 	string($date-time)
expiryDays	|   integer($int32)
id  | 	string($uuid)
client  | 	[ClientShortResponse](client.md)
warehouse | 	[WarehouseShortResponse](warehouse.md)
zone  | 	[ZoneShortResponse](zone.md)
location  | 	[LocationShortResponse](warehouse-location.md)
product    |	[ProductShortResponse](product.md)
----------------------------

## Stock inventory summary
* **GET:** `/stock-inventory/summary`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
ClientId      | array[string]   | true |
WarehouseId   | array[string]   | true |
ZoneId        | array[string]   | - |
LocationId    | array[string]   | - |
ProductId     | array[string]   | - |
Status        | array[string]   | - |

### Responses
> Example Value
```json
{
  "onHandQty": 0,
  "stagingQty": 0,
  "reservedQty": 0,
  "pickingQty": 0,
  "packingQty": 0,
  "transferQty": 0,
  "unusableQty": 0,
  "availableQty": 0,
}
```
> Schema

name | type |
----- | ----- |
onHandQty   | 	integer($int32)
stagingQty  | 	integer($int32)
reservedQty   | 	integer($int32)
pickingQty  | 	integer($int32)
packingQty  | 	integer($int32)
transferQty   | 	integer($int32)
unusableQty   | 	integer($int32)
availableQty  | 	integer($int32)
----------------------------
