
# Tracking Data
[Back to readme](../README.md)

### Request body
> Example Value
```json
{
  "orderCode": "string",
  "nyxCode": "string",
  "statusCode": "string",
  "statusName": "string",
  "stationCode": "string",
  "stationName": "string",
  "trackingCode": "string",
  "trackingName": "string",
  "trackingPod": "string",
  "trackingLocation": "string",
  "trackingDate": "2022-02-09T02:59:27.357Z",
  "trackingRemark": "string"
}
```
> Schema

name | type |
----- | ----- |
orderCode |	string |
nyxCode |	string |
statusCode |	string |
statusName |	string |
stationCode |	string |
stationName |	string |
trackingCode |	string |
trackingName |	string |
trackingPod |	string |
trackingLocation |	string |
trackingDate |	string($date-time) |
trackingRemark |	string |