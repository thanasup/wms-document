# Warehouse Zone
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `zoneId`

## Warehouse short information
* **GET:** `/warehouse-zone/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt     | string          | - |
Code          | string          | - |
Name          | string          | - |
WarehouseId   | array[string]   | true |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "name": "string"
  }
]
```
> Schema

name | type |
----- | ----- |
id	               | string($uuid) |
code	             | string   |
name	             | string   |

----------------------------
