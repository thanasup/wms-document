# Authentication
[Back to readme](../README.md)

## Authenticate
* **POST:** `/accounts/authenticate`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "username": "username",
  "password": "password",
  "isPersistent": true
}
```
> Schema

name | type | required 
----- | ----- | ----- |
username	   | string  | true |
password	   | string  | true |
isPersistent | boolean | true |

--------------
### Responses
> Example Value
```json
{
  "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "title": "string",
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "role": "string",
  "created": "2021-10-27T04:28:27.323Z",
  "updated": "2021-10-27T04:28:27.323Z",
  "isVerified": true,
  "token": "string",
  "sessionToken": "string",
  "avatar": "string"
}
```
> Schema

name | type | nullable 
----- | ----- | ----- |
accountId	   | string($uuid)      | false |
clientId     | string($uuid)      | true  |
title	       | string             | true  |
firstName	   | string             | true  |
lastName	   | string             | true  |
email	       | string             | true  |
role	       | string             | true  |
created	     | string($date-time) | false |
updated	     | string($date-time) | true  |
isVerified	 | boolean            | false |
token	       | string             | true  |
sessionToken | string             | true  |
avatar	     | string             | true  |

--------------

## Refresh token
* **POST:** `/accounts/refresh-token`
* **Media type:** `application/json` Controls Accept header.
### Responses
> Example Value

> Schema

--------------
## Change password
* **POST:** `/accounts/authenticate`
* **Media type:** `application/json` Controls Accept header.
### Responses
> Example Value

> Schema

--------------