# Warehouse location
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `locationId`

> **Reference:**
> * get `warehouseId` from response of [Warehouse](warehouse.md)

## Warehouse location
* **GET:** `/warehouse-location/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt         | string           | false |
Code              | array[string]    | false |
IsReceive         | boolean          | false |
IsPicking         | boolean          | false |
IsTransfer        | boolean          | false |
IsDamage          | boolean          | false |
IsInspect         | boolean          | false |
IsOnlyOneArticle  | boolean          | false |
WarehouseId       | array[string]    | true |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "zoneName": "string",
    "locked": true,
    "isReceive": true,
    "isPicking": true,
    "isTransfer": true,
    "isOnlyOneArticle": true,
    "isDamage": true,
    "isInspect": true
  }
]
```

> Schema

name | type |
----- | ----- |
id          	    | string($uuid) |
code          	  | string |
zoneName          |	string |
locked          	| boolean |
isReceive         |	boolean |
isPicking         |	boolean |
isTransfer        | boolean |
isOnlyOneArticle  | boolean |
isDamage          |	boolean |
isInspect         |	boolean |

----------------------------