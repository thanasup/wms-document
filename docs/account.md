# Account
[Back to readme](../README.md)

## Account short infomation
> Example Value
```json
{
  "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "empCode": "string",
  "title": "string",
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "empFullName": "string"
}
```
> Schema

name | type | nullable 
----- | ----- | ----- |
accountId 	| string($uuid)   | false |
empCode 	  | string          | true  |
title 	    | string          | true  |
firstName 	| string          | true  |
lastName  	| string          | true  |
email 	    | string          | true  |
empFullName |	string          | true  |

--------------