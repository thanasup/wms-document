# Advance Shipment Notice
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `inboundId`
> * documentType
>   * `0` = Normal
>   * `1` = Return
>   * `2` = FreeOfCharge 
> * containerType
>   * `0` = Container
>   * `1` = Pallet
>   * `2` = Carton 
> * shippingMethod
>   * `0` = Air
>   * `1` = Sea
>   * `2` = Vehicle
> * status
>   * `DF` = Draft
>   * `OP` = Open

> **Reference:**
> * get `warehouseId` from response of [Warehouse](warehouse.md)
> * get `clinentId` from response of [Authenticate](authentication.md)
> * get `vendorId` from response of [Transport Partner](transport-partner.md)
> * get `productId` from response of [Product](product.md)
> * get `uomId` from response of [Product Unit Of Measure](product-uom.md)

## ASN infomation
* **GET:** `/advance-shipment-notice/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Response
* **Code:** `200` _Success_
> Example Value
```json
{
  "documentAsnDate": "2021-10-27T16:23:18.996Z",
  "refNo": "string",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "documentType": 0,
  "poNo": "string",
  "soNo": "string",
  "vendorId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "vendorContactPerson": "string",
  "asnStatus": "string",
  "shippingMethod": 0,
  "voyage": "string",
  "vessel": "string",
  "estimate": "2021-10-27T16:23:18.996Z",
  "containerType": 0,
  "description": "string",
  "items": [ ... ASN Items ],
  "vendorName": "string",
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "actual": "2021-10-27T16:23:18.996Z",
  "confirmAsnDate": "2021-10-27T16:23:18.996Z",
  "confirmAsnBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "createAsnBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "userCreateAsn": {
    ... Account short infomation
  },
  "userConfirmAsn": {
    ... Account short infomation
  },
  "warehouse": {
    ... Warehouse short infomation
  },
  "vendor": {
    ... Transport Partner short infomation
  },
  "client": {
    ... Client short response
  },
  "asnNo": "string",
  "grnNo": "string",
  "importAsnDate": "2021-10-27T16:23:18.996Z",
  "importGrnDate": "2021-10-27T16:23:18.996Z"
}
```
> Schema

name | type |
----- | ----- |
documentAsnDate	     | string($date-time) |
refNo	               | string |
warehouseId	         | string($uuid) |
clientId	           | string($uuid) |
documentType	       | integer($int32) |
poNo	               | string |
soNo	               | string |
vendorId	           | string($uuid) |
vendorContactPerson	 | string |
asnStatus	           | string |
shippingMethod	     | integer($int32) |
voyage	             | string |
vessel	             | string |
estimate	           | string($date-time) |
containerType	       | integer($int32) |
description	         | string |
items	               | [[...ASN Items]](asn-items.md) |
vendorName	         | string |
id	                 | string($uuid) |
actual	             | string($date-time) |
confirmAsnDate	     | string($date-time) |
confirmAsnBy	       | string($uuid) |
createAsnBy	         | string($uuid) |
userCreateAsn	       | [ShortAccountResponse](account.md) |
userConfirmAsn	     | [ShortAccountResponse](account.md) |
warehouse	           | [WarehouseShortResponse](warehouse.md) |
vendor	             | [VendorShortResponse](transport-partner.md) |
client	             | [ClientShortResponse](client.md) |
asnNo	               | string |
grnNo	               | string |
importAsnDate	       | string($date-time) |
importGrnDate	       | string($date-time) 

--------------

## Create ASN
* **POST:** `/advance-shipment-notice`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "documentAsnDate": "2021-10-27T09:32:45.448Z",
  "refNo": "string",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "documentType": 0
}
```
> Schema

name | type | required | maxLength |
----- | ----- | ----- | ----- |
documentAsnDate   | string          | true  | -   |
refNo             | string          | -     | 50 |
warehouseId       | string          | true  | -   |
clientId          | string          | true  | -   |
documentType      | integer($int32) | true  | -   |

### Response
* **Code:** `200` _Success_
> Example Value
> * Same to ASN infomation `response example value`

> Schema
> * Same to ASN infomation  `response example schema`

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Update ASN
* **PUT:** `/advance-shipment-notice/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Request body
> Example Value
```json
{
  "documentAsnDate": "2021-10-27T16:49:17.403Z",
  "refNo": "string",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "documentType": 0,
  "poNo": "string",
  "soNo": "string",
  "vendorId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "vendorContactPerson": "string",
  "asnStatus": "string",
  "shippingMethod": 0,
  "voyage": "string",
  "vessel": "string",
  "estimate": "2021-10-27T16:49:17.403Z",
  "containerType": 0,
  "description": "string",
  "items": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "inboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "batchNo": "string",
      "lotNo": "string",
      "palletNo": "string",
      "barcode": "string",
      "mfgDate": "2021-10-27T16:49:17.403Z",
      "expiryDate": "2021-10-27T16:49:17.403Z",
      "orderQty": 0,
    }
  ]
}
```

> Schema

name | type | required | maxLength |
----- | ----- | ----- | ----- |
documentAsnDate	      | string($date-time)              | true | -  |
refNo	                | string                          | -    | 50 |
warehouseId           | string($uuid)                   | true | -  |
clientId              |	string($uuid)                   | true | -  |
documentType          | integer($int32)                 | true | -  |
poNo	                | string                          | -    | 50 |
soNo	                | string                          | -    | 50 |
vendorId	            | string($uuid)                   | -    | -  |
vendorContactPerson	  | string                          | -    | 50 |
asnStatus	            | string                          | true | -  |
shippingMethod	      | integer($int32)                 | -    | -  |
voyage	              | string                          | -    | 250 |
vessel	              | string                          | -    | 100 |
estimate	            | string($date-time)              | -    | -  |
containerType	        | integer($int32)                 | -    | -  |
description	          | string                          | -    | 250 |
items	                | [[... AsnItemsRequest]](asn-items.md)  | -    | -  |

### Response
* **Code:** `200` _Success_
> Example Value
> * Same to ASN infomation `response example value`

> Schema
> * Same to ASN infomation  `response example schema`

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```
--------------

## Cancel ASN
* **PATCH:** `/advance-shipment-notice/cancel/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |

### Response
* **Code:** `200` _Success_
--------------

## Import batch ASN
* **POST:** `/advance-shipment-notice/import-batch`
* **Media type:** `application/json` Controls Accept header.
> **Note:** 
>
> this path use for upload batch file, allowed file type `.xlsx`
### Request body
* **Content:** `multipart/form-data`

> Schema

name | type | required | default |
----- | ----- | ----- | ----- |
ContentRowStart | integer($int32) | | 2 |
File            | string($binary) | true  |   |

### Response
* **Code:** `200` _Success_

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```
--------------

## Confirm selected batch ASN
* **PUT:** `/advance-shipment-notice/confirm-selected-batch`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |

### Response
* **Code:** `200` _Success_

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Delete selected batch ASN
* **PUT:** `/advance-shipment-notice/delete-selected-batch`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |

### Response
* **Code:** `200` _Success_

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```
--------------