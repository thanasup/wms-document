# Client
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with the ones of your `clientId`
>
>      **Your `clientId` assigned by Namyong

> **Reference:**
> * get `id` with `clientId` from response of [Authenticate](authentication.md)
> * get `ShortAccountResponse` from [Account short response](account.md)

## Client infomation
* **GET:** `/client/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |

### Responses
> Example Value
```json
{
  "code": "string",
  "name": "string",
  "description": "string",
  "phone": "string",
  "tel": "string",
  "fax": "string",
  "email": "string",
  "address": "string",
  "countryCode": "string",
  "provinceCode": "string",
  "cityCode": "string",
  "areaCode": "string",
  "postalCode": "string",
  "isActive": true,
  "isDefault": true,
  "keyAccount": "string",
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "userCreateBy": {
    "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "empCode": "string",
    "title": "string",
    "firstName": "string",
    "lastName": "string",
    "email": "string",
    "empFullName": "string"
  },
  "userUpdateBy": {
    "accountId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "empCode": "string",
    "title": "string",
    "firstName": "string",
    "lastName": "string",
    "email": "string",
    "empFullName": "string"
  },
  "createBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "updateBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "created": "2021-10-27T08:13:58.767Z",
  "updated": "2021-10-27T08:13:58.767Z"
}
```
> Schema

name | type | nullable 
----- | ----- | ----- |
code	        | string               | true |
name	        | string               | true |
description	  | string               | true |
phone	        | string               | true |
tel	          | string               | true |
fax	          | string               | true |
email	        | string               | true |
address	      | string               | true |
countryCode	  | string               | true |
provinceCode  | string               | true |
cityCode	    | string               | true |
areaCode	    | string               | true |
postalCode	  | string               | true |
isActive	    | boolean              | false |
isDefault	    | boolean              | false |
keyAccount	  | string               | true |
id	          | string($uuid)        | false |
userCreateBy	| ShortAccountResponse | false |
userUpdateBy	| ShortAccountResponse | true |
createBy	    | string($uuid)        | false |
updateBy	    | string($uuid)        | true |
created	      | string($date-time)   | false |
updated	      | string($date-time)   | true |

----------------------------

## Client short information
* **GET:** `/client/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt     | string          | false |
Code          | string          | false |
Name          | string          | false |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "name": "string",
    "keyAccount": "string",
    "showShipperAddress": true,
    "showEquipment": true
  }
]
```
> Schema ClientShortResponse

name | type |
----- | ----- |
id	               | string($uuid) |
code	             | string   |
name	             | string   |
keyAccount	        | string  |
showShipperAddress	| boolean  |
showEquipment       | boolean |


----------------------------
