# Delivery Order Address
[Back to readme](../README.md)
> **Note:**
> * addressType
>   * `0` = Shiper
>   * `1` = Pickup
>   * `2` = Receiver
> * if you want to create new address you can set `createNewClientAddress` is `true`

> **Reference:**
> * get `outboundId` from response of [Delivery Order](delivery-order.md)
> * get `countryCode` from response of [Regional](regional.md)
> * get `provinceCode` from response of [Regional](regional.md)
> * get `cityCode` from response of [Regional](regional.md)
> * get `areaCode` from response of [Regional](regional.md)
> * get `postalCode` from response of [Regional](regional.md)

### Request body
> Example Value
```json
{
  "isDefault": true,
  "addressType": 0,
  "address": "string",
  "countryCode": "string",
  "countryName": "string",
  "provinceCode": "string",
  "provinceName": "string",
  "cityCode": "string",
  "cityName": "string",
  "areaCode": "string",
  "areaName": "string",
  "postalCode": "string",
  "tel": "string",
  "phone": "string",
  "email": "string",
  "createNewClientAddress": true,
  "contactPerson": "string"
}
```
> Schema

name | type| required | maxLength |
----- | ----- |----- | ----- |
isDefault             	| boolean         |  -  |   - |
addressType             | integer($int32) | true|   - |
address                 | string          | true| 255 |
countryCode             | string          | true|   - |
countryName             | string          |  -  |   - |
provinceCode            | string          | true|   - |
provinceName            | string          |  -  |   - |
cityCode                | string          | true|   - |
cityName                | string          |  -  |   - |
areaCode                | string          | true|   - |
areaName              	| string          |  -  |   - |
postalCode              | string          | true|   - |
tel             	      | string          |  -  | 100 |
phone             	    | string          |  -  | 100 |
email             	    | string          |  -  | 100 |
outboundId              | string($uuid)   | true|   - |
createNewClientAddress  | boolean         |  -  |   - |
contactPerson           | string          |  -  | 100 |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "isDefault": true,
    "addressType": 0,
    "address": "string",
    "countryCode": "string",
    "countryName": "string",
    "provinceCode": "string",
    "provinceName": "string",
    "cityCode": "string",
    "cityName": "string",
    "areaCode": "string",
    "areaName": "string",
    "postalCode": "string",
    "tel": "string",
    "phone": "string",
    "email": "string",
    "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "contactPerson": "string",
    "createNewClientAddress": true
  }
]
```

> Schema

name | type| required | maxLength |
----- | ----- |----- | ----- |
id              	      | string($uuid)   |  -  |   - |
isDefault             	| boolean         |  -  |   - |
addressType             | integer($int32) | true|   - |
address                 | string          | true| 255 |
countryCode             | string          | true|   - |
countryName             | string          |  -  |   - |
provinceCode            | string          | true|   - |
provinceName            | string          |  -  |   - |
cityCode                | string          | true|   - |
cityName                | string          |  -  |   - |
areaCode                | string          | true|   - |
areaName              	| string          |  -  |   - |
postalCode              | string          | true|   - |
tel             	      | string          |  -  | 100 |
phone             	    | string          |  -  | 100 |
email             	    | string          |  -  | 100 |
outboundId              | string($uuid)   | true|   - |
contactPerson           | string          |  -  | 100 |
createNewClientAddress  | boolean         |  -  |   - |