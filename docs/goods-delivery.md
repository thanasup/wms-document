# Goods Delivery
[Back to readme](../README.md)


## Order Tracking
* **GET:** `/goods-delivery/tracking/{code}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
code    | string | true |

> **Note:**
> * Note that the `code` can be use replaced with `nyxCode` or [`documentNo`](delivery-order.md)

### Responses
* **Code:** `200` _Success_
> Example Value
```json
{
  "orderCode": "string",
  "nyxCode": "string",
  "origin": "string",
  "destination": "string",
  "agentCode": "string",
  "agentName": "string",
  "firstmileTracking": "string",
  "agentTracking": "string",
  "error": "string",
  "trackingData": [
    {
      "orderCode": "string",
      "nyxCode": "string",
      "statusCode": "string",
      "statusName": "string",
      "stationCode": "string",
      "stationName": "string",
      "trackingCode": "string",
      "trackingName": "string",
      "trackingPod": "string",
      "trackingLocation": "string",
      "trackingDate": "2022-02-09T02:59:27.357Z",
      "trackingRemark": "string"
    }
  ]
}
```

> Schema

name | type |
----- | ----- |
orderCode |	string
nyxCode |	string
origin |	string
destination |	string
agentCode |	string
agentName |	string
firstmileTracking |	string
agentTracking |	string
error |	string
trackingData | [[...TrackingData]](tracking-data.md)