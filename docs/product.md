# Product
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `productId`
> * productType
>   * `0` = ItemArticle
>   * `1` = ServiceCharge
>   * `2` = Equipment 

> **Reference:**
> * get `ClientId` from response of [Authenticate](authentication.md)
> * get `CategoryId` from response of [Product Category](product-category.md)
> * get `ProductBrandId` from response of [Product Brand](product-brand.md)

## Product short infomation
* **GET** `/product/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required | 
----- | ----- | ----- | 
ExcludeProductInStock | boolean         | false | 
SearchTxt         | string          | false |
Code              | string          | false |
Name              | string          | false |
ClientId          | array[string]   | true |
CategoryId        | array[string]   | false |
ProductBrandId    | array[string]   | false |
ProductType       | array[integer]  | false |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "name": "string",
    "nameTh": "string",
    "description": "string",
    "barcode": "string",
    "shelfLife": 0,
    "length": 0,
    "width": 0,
    "height": 0,
    "weight": 0,
    "productType": 0,
    "uom": {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "code": "string",
      "name": "string",
      "symbol": "string",
      "noOfUnit": 0,
      "precision": 0
    }
  }
]
```
> Schema

name | type |
----- | ----- | 
id	         | string($uuid) |
code	       | string |
name	       | string |
nameTh	     | string |
description	 | string |
barcode	     | string |
shelfLife	   | integer($int32) |
length	     | number($double) |
width	       | number($double) |
height	     | number($double) |
weight	     | number($double) |
productType	 | integer($int32) |

--------------
