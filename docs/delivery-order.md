# Delivery Order
[Back to readme](../README.md)
> **Note:**
> * Note that the `id` should be replaced with `outboundId`
> * Status
>   * `DF` = Draft
>   * `OP` = Open

> **Reference:**
> * get `warehouseId` from response of [Warehouse](warehouse.md)
> * get `clinentId` from response of [Authenticate](authentication.md)
> * get `vendorId` from response of [Transport Partner](transport-partner.md)
> * get `productId` from response of [Product](product.md)
> * get `uomId` from response of [Product Unit Of Measure](product-uom.md)

## DO infomation
* **GET:** `/delivery-order/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
{
  "documentDate": "2021-10-27T17:11:15.397Z",
  "soNo": "string",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "refNo": "string",
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "status": "string",
  "documentNo": "string",
  "clientContactPerson": "string",
  "description": "string",
  "orderedBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "receiveLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "estimate": "2021-10-27T17:11:15.397Z",
  "acctual": "2021-10-27T17:11:15.397Z",
  "vendorId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "vendorContactPerson": "string",
  "stagingPickLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "requiredVas": true,
  "requiredQa": true,
  "address": [ ... DO Address ],
  "createBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "ginNo": "string",
  "confirmAllocateDate": "2021-10-27T17:11:15.397Z",
  "confirmAllocateBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmPrintPickingDate": "2021-10-27T17:11:15.397Z",
  "confirmPrintPickingBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmPickingDate": "2021-10-27T17:11:15.397Z",
  "confirmPickingBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "pickingBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmVasDate": "2021-10-27T17:11:15.397Z",
  "confirmVasBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmQaDate": "2021-10-27T17:11:15.397Z",
  "confirmQaBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmPackingDate": "2021-10-27T17:11:15.397Z",
  "confirmPackingBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmIssueDate": "2021-10-27T17:11:15.397Z",
  "confirmIssueBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "confirmDispatchDate": "2021-10-27T17:11:15.397Z",
  "confirmDispatchBy": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "items": [ ... DO Items ],
  "client": {
    ... Client short response
  },
  "vendor": {
    ... Transport partner short response
  },
  "warehouse": {
    ... Warehouse short response
  },
  "receiveLocation": {
    ... Warehouse location short response
  },
  "stagingPickLocation": {
    ... Warehouse location short response
  },
  "userOrdered": {
    ... Account short response
  },
  "userPicking": {
    ... Account short response
  },
  "userConfirmAllocate": {
   ... Account short response
  },
  "userConfirmPrintPicking": {
    ... Account short response
  },
  "userConfirmPicking": {
    ... Account short response
  },
  "userConfirmVas": {
    ... Account short response
  },
  "userConfirmQa": {
    ... Account short response
  },
  "userConfirmPacking": {
    ... Account short response
  },
  "userConfirmIssue": {
    ... Account short response
  },
  "userConfirmDispatch": {
    ... Account short response
  },
  "stockPacking": [ ... DO Stock Packing],
  "allocates": [ ... DO Stock Allocate],
  "hasBackorder": true,
  "process": "string",
  "processBadge": 0
}
```

> Schema

name | type |
----- | ----- |
documentDate            | string($date-time)
soNo              	    | string
clientId                | string($uuid)
warehouseId             | string($uuid)
id            	        | string($uuid)
status                  | string
clientContactPerson     | string
description           	| string
vendorContactPerson     | string
requiredVas             | boolean
requiredQa              | boolean
address           	    | [[... DO Address]](delivery-order-address.md)
items           	      | [[... DO Items]](delivery-order-items.md)
client            	    | [Client](client.md)
vendor            	    | [VendorShortResponse](transport-partner.md)
warehouse           	  | [WarehouseShortResponse](warehouse.md)
receiveLocation         | [LocationShortResponse](warehouse-location.md)
stagingPickLocation     | [LocationShortResponse](warehouse-location.md)
userOrdered           	| [ShortAccountResponse](account.md)
userPicking           	| [ShortAccountResponse](account.md)
userConfirmAllocate     | [ShortAccountResponse](account.md)
userConfirmPrintPicking | [ShortAccountResponse](account.md)
userConfirmPicking      | [ShortAccountResponse](account.md)
userConfirmVas          | [ShortAccountResponse](account.md)
userConfirmQa           |	[ShortAccountResponse](account.md)
userConfirmPacking      | [ShortAccountResponse](account.md)
userConfirmIssue        | [ShortAccountResponse](account.md)
userConfirmDispatch     | [ShortAccountResponse](account.md)
stockPacking            |	[[... DO Stock Packing]](stock-packing.md)
allocates           	  | [[... DO Stock Allocate]](stock-allocate.md)
hasBackorder            |	boolean
processBadge            |	integer($int32)

--------------

## Create DO
* **POST:** `/delivery-order`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "documentDate": "2021-10-27T12:56:28.397Z",
  "soNo": "string",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "refNo": "string"
}
```
> Schema

name | type | required | maxLength |
----- | ----- | ----- | ----- |
documentAsnDate   | string          | true  | -  |
soNo              | string          | -     | 50 |
warehouseId       | string          | true  | -  |
clientId          | string          | true  | -  |
refNo             | string          | -     | -  |

### Responses
* **Code:** `200` _Success_
> Example Value
> * Same to DO infomation `response example value`

> Schema
> * Same to DO infomation  `response example schema`

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Update DO
* **PUT:** `/delivery-order/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Request body
> Example Value
```json
{
  "documentDate": "2021-10-27T18:17:09.640Z",
  "soNo": "string",
  "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "refNo": "string",
  "receiveLocationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "status": "string",
  "description": "string",
  "estimate": "2021-10-27T18:17:09.640Z",
  "vendorId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "vendorContactPerson": "string",
  "address": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "isDefault": true,
      "addressType": 0,
      "address": "string",
      "countryCode": "string",
      "countryName": "string",
      "provinceCode": "string",
      "provinceName": "string",
      "cityCode": "string",
      "cityName": "string",
      "areaCode": "string",
      "areaName": "string",
      "postalCode": "string",
      "tel": "string",
      "phone": "string",
      "email": "string",
      "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "contactPerson": "string",
      "createNewClientAddress": true
    }
  ],
  "items": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "orderQty": 0,
      "price": 0
    }
  ]
}
```

> Schema

name | type | required | maxLength |
----- | ----- | ----- | ----- |
documentDate          | string($date-time)                            | true | -    |
soNo          	      | string                                        | -    |  50  |
clientId              | string($uuid)                                 | true | -    |
warehouseId           | string($uuid)                                 | true | -    |
refNo         	      | string                                        | -    | -    |
receiveLocationId     | string($uuid)                                 | -    | -    |
status                | string                                        | true | -    |
description           | string                                        | -    |  250 |
estimate          	  | string($date-time)                            | -    | -    |
vendorId          	  | string($uuid)                                 | -    | -    |
vendorContactPerson   | string                                        | -    |  100 |
address         	    | [[...DO Address]](delivery-order-address.md)  | true | -    |
items                 | [[...DO Items]](delivery-order-items.md)      | true | -    |

### Responses
* **Code:** `200` _Success_
> Example Value
> * Same to DO infomation `response example value`

> Schema
> * Same to DO infomation  `response example schema`

* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Cancel DO
* **PATCH:** `/delivery-order/cancel/{id}`
* **Media type:** `application/json` Controls Accept header.
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Response
* **Code:** `200` _Success_

--------------

## Reverse draft DO
* **PATCH:** `/delivery-order/reverse-draft/{id}`
* **Media type:** `application/json` Controls Accept header.
> **Note:** 
>
> this path use for reverse status `Open` to `Draft`
### Parameter `Path`
name | type | required 
----- | ----- | ----- |
id    | string($uuid) | true |
### Response
* **Code:** `200` _Success_

--------------

## Import batch DO
* **POST:** `/delivery-order/import-batch`
* **Media type:** `application/json` Controls Accept header.
> **Note:** 
>
> this path use for upload batch file, allowed file type `.xlsx`
### Request body
* **Content:** `multipart/form-data`

> Schema

name | type | required | default |
----- | ----- | ----- | ----- |
ContentRowStart | integer($int32) | -     | 2  |
File            | string($binary) | true  | -  |
### Response
* **Code:** `200` _Success_
* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Confirm selected batch DO
* **PUT:** `/delivery-order/confirm-selected-batch`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |
### Response
* **Code:** `200` _Success_
* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Delete with DO select
* **PUT:** `/delivery-order/delete-selected-batch`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |
### Response
* **Code:** `200` _Success_
* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Opening with DO select
* **PUT:** `/confirm-opening-delivery-order-selected`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |
### Response
* **Code:** `200` _Success_
* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------

## Approve with DO select
* **PUT:** `/confirm-approve-delivery-order-selected`
* **Media type:** `application/json` Controls Accept header.
### Request body
> Example Value
```json
{
  "id": [
    "3fa85f64-5717-4562-b3fc-2c963f66afa6"
  ]
}
```
> Schema

name | type | required 
----- | ----- | ----- |
id    | array[string] | true |
### Response
* **Code:** `200` _Success_
* **Code:** `400` _Bad Request_
> Example Value
```
{
  "message": "message errors"
}
```

--------------