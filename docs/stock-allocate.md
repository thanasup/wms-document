# Stock Allocate
[Back to readme](../README.md)
> **Reference:**
> * [Warehouse short infomation](warehouse.md)
> * [Warehouse location short infomation](warehouse-location.md)
> * [UOM short infomation](uom.md)
> * [Product short infomation](product.md)

> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "inboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "grnNo": "string",
    "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "outboundDtId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "ginNo": "string",
    "batchNo": "string",
    "lotNo": "string",
    "barcode": "string",
    "palletNo": "string",
    "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "clientId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "zoneId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "status": "string",
    "issueQty": 0,
    "scanQty": 0,
    "stockDate": "2021-10-27T17:11:15.397Z",
    "expiryDate": "2021-10-27T17:11:15.397Z",
    "productName": "string",
    "uomName": "string",
    "client": {
      ... Client short response
    },
    "warehouse": {
      ... Warehouse short infomation
    },
    "zone": {
      ... Zone short response
    },
    "location": {
      ... Warehouse location short infomation
    },
    "uom": {
      ... UOM short infomation
    },
    "product": {
      ... Product short infomation
    }
  }
]
```

> Schema

name | type |
----- | ----- |
id          	|  string($uuid)
inboundId     |  string($uuid)
grnNo         |  string
outboundId    |  string($uuid)
outboundDtId  |  string($uuid)
ginNo         |  string
batchNo       |	string
lotNo         |  string
barcode       |	string
palletNo      |  string
warehouseId   |  string($uuid)
clientId      |  string($uuid)
zoneId        |	string($uuid)
locationId    |  string($uuid)
productId     |  string($uuid)
uomId         |  string($uuid)
status        |	string
issueQty      |  integer($int32)
scanQty       |	integer($int32)
stockDate     |  string($date-time)
expiryDate    |  string($date-time)
productName   |  string
uomName       |	string
client        |	[Client](client.md)
warehouse     |  [WarehouseShortResponse](warehouse.md)
zone          |  [ZoneShortResponse](zone.md)
location      |  [LocationShortResponse](warehouse-location.md)
uom         	|  [UomShortResponse](uom.md)
product       |	[ProductShortRespons](product.md)

