# Stock Packing
[Back to readme](../README.md)
> **Reference:**
> * [UOM short infomation](uom.md)
> * [Product short infomation](product.md)

> Example Value
```json
[
    {
      "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "packageCode": "string",
      "parentPackageCode": "string",
      "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "grnNo": "string",
      "batchNo": "string",
      "lotNo": "string",
      "palletNo": "string",
      "barcode": "string",
      "issueQty": 0,
      "price": 0,
      "totalPrice": 0,
      "length": 0,
      "width": 0,
      "height": 0,
      "weight": 0,
      "orderCode": "string",
      "nyxCode": "string",
      "firstmileTracking": "string",
      "agentTracking": "string",
      "statusCodeRef": "string",
      "statusNameRef": "string",
      "agentCode": "string",
      "agentName": "string",
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "productName": "string",
      "uomName": "string",
      "uom": {
        ... UOM short infomation
      },
      "product": {
        ... Product short infomation
      }
    }
  ]
```

> Schema

name | type |
----- | ----- |
outboundId         |	string($uuid)
packageCode        |	string
parentPackageCode  |	string
productId          |	string($uuid)
uomId              |	string($uuid)
grnNo              |	string
batchNo            |	string
lotNo              |	string
palletNo           |	string
barcode            |	string
issueQty           |	integer($int32)
price              |	number($double)
totalPrice         |	number($double)
length             |	number($double)
width              |	number($double)
height             |	number($double)
weight             |	number($double)
orderCode          |	string
nyxCode            |	string
firstmileTracking  |	string
agentTracking      |	string
statusCodeRef      |	string
statusNameRef      |	string
agentCode          |	string
agentName          |	string
id                 |	string($uuid)
productName        |	string
uomName            |	string
uom                |	[UomShortResponse](uom.md)
product            |	[ProductShortResponse](product.md)