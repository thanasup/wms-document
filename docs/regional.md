# Regional
[Back to readme](../README.md)

## Regional with country
* **GET:** `https://nos.namyongwex.com/nos-api/Enquiries/getCountry`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
Country         | string          | true |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "countryCode": "string",
    "countryName": "string",
    }
]
```
> Schema

name | type |
----- | ----- |
countryCode | string
countryName | string

--------------


## Regional with province
* **GET:** `https://nos.namyongwex.com/nos-api/Enquiries/getProvince`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
countryCode         | string          | true |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "provinceCode": "string",
    "countryCode": "string",
    "provinceName": "string",
    "provinceNameTh": "string",
    "countryName": "string",
    }
]
```
> Schema

name | type |
----- | ----- |
provinceCode | string
provinceName | string
provinceNameTh | string
countryCode | string
countryName | string

--------------

## Regional with city
* **GET:** `https://nos.namyongwex.com/nos-api/Enquiries/getCity`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
countryCode  | string          | true |
provinceCode | string          | true |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "cityCode": "string",
    "cityName": "string",
    "cityNameTh": "string",
    "countryCode": "string",
    "provinceCode": "string",
    "countryName": "string",
    "provinceName": "string",
    "provinceNameTh": "string",
    }
]
```
> Schema

name | type |
----- | ----- |
cityCode | string
cityName | string
cityNameTh | string
countryCode | string
provinceCode | string
countryName | string
provinceName | string
provinceNameTh | string

--------------

## Regional with area
* **GET:** `https://nos.namyongwex.com/nos-api/Enquiries/getArea`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
countryCode      | string          | true |
provinceCode     | string          | true |
cityCode         | string          | true |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "areaCode": "string",
    "areaName": "string",
    "areaNameTh": "string",
    "countryCode": "string",
    "provinceCode": "string",
    "cityCode": "string",
    "countryName": "string",
    "provinceName": "string",
    "provinceNameTh": "string",
    "cityName": "string",
    "cityNameTh": "string",
    }
]
```
> Schema

name | type |
----- | ----- |
areaCode | string
areaName | string
areaNameTh | string
countryCode | string
provinceCode | string
cityCode | string
countryName | string
provinceName | string
provinceNameTh | string
cityName | string
cityNameTh | string

--------------

## Regional with postal code
* **GET:** `https://nos.namyongwex.com/nos-api/Enquiries/getPostalCode`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
countryCode      | string          | true |
provinceCode     | string          | true |
cityCode         | string          | true |
postalCode       | string          | true |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "postalCode": "string",
    "countryCode": "string",
    "provinceCode": "string",
    "cityCode": "string",
    "areaCode": "string",
    "countryName": "string",
    "provinceName": "string",
    "cityName": "string",
    "areaName": "string",
    "countryNameTh": "string",
    "provinceNameTh": "string",
    "cityNameTh": "string",
    "areaNameTh": "string",
    }
]
```
> Schema

name | type |
----- | ----- |
postalCode | string
countryCode | string
provinceCode | string
cityCode | string
areaCode | string
countryName | string
provinceName | string
cityName | string
areaName | string
countryNameTh | string
provinceNameTh | string
cityNameTh | string
areaNameTh | string

--------------
