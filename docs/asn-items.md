# ASN Items
[Back to readme](../README.md)

> **Reference:**
> * get `productId` from response of [Product](product.md)
> * get `uomId` from response of [Product Unit Of Measure](product-uom.md)

## Asn items
> Example Value
```json
[
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "inboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "batchNo": "string",
      "lotNo": "string",
      "palletNo": "string",
      "barcode": "string",
      "mfgDate": "2021-10-27T16:23:18.996Z",
      "expiryDate": "2021-10-27T16:23:18.996Z",
      "orderQty": 0,
      "productName": "string",
      "uomName": "string",
      "product": {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "code": "string",
        "name": "string",
        "nameTh": "string",
        "description": "string",
        "barcode": "string",
        "shelfLife": 0,
        "length": 0,
        "width": 0,
        "height": 0,
        "weight": 0,
        "productType": 0,
        "uom": {
          "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "code": "string",
          "name": "string",
          "symbol": "string",
          "noOfUnit": 0,
          "precision": 0
        }
      },
      "uom": {
        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "code": "string",
        "name": "string",
        "symbol": "string",
        "noOfUnit": 0,
        "precision": 0
      }
    }
  ]
```
> Schema AsnItemsRequest

name | type | required | maxLength | minimum |
----- | ----- | ----- | ----- | ----- |
id	         | string($uuid)                      | -    | -   | - | 
inboundId    | string($uuid)                      | true | -   | - |
productId    | string($uuid)                      | true | -   | - |
uomId        | string($uuid)                      | true | -   | - |
batchNo	     | string                             | -    | 100 | - |
lotNo	       | string                             | -    | 100 | - |
palletNo	   | string                             | -    | 100 | - |
barcode	     | string                             | -    | 100 | - |
mfgDate	     | string($date-time)                 | -    | -   | - |
expiryDate	 | string($date-time)                 | -    | -   | - |
orderQty	   | integer($int32)                    | true | -   | 0 |
productName	 | string                             | -    | -   | - | 
uomName	     | string                             | -    | -   | - | 
product	     | [ProductShortResponse](product.md) | -    | -   | - | 
uom	         | [UomShortResponse](uom.md)         | -    | -   | - | 

--------------