# Delivery Order Items
[Back to readme](../README.md)
> **Reference:**
> * get `productId` from response of [Product](product.md)
> * get `uomId` from response of [Product Unit Of Measure](product-uom.md)
> * [Warehouse location short infomation](warehouse-location.md)
> * [UOM short infomation](uom.md)
> * [Product short infomation](product.md)

### Request body
> Example Value
```json
{
  "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "orderQty": 0,
  "price": 0
}
```
> Schema

name | type| required | maxLength |
----- | ----- |----- | ----- |
productId     | string($uuid)                                   | true |
uomId     	  | string($uuid)                                   | true |
orderQty      | integer($int32)                                 | true |
price     	  | number($double)                                 | true |

### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "outboundId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "warehouseId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "zoneId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "locationId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "productId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "uomId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "grnNo": "string",
    "batchNo": "string",
    "lotNo": "string",
    "palletNo": "string",
    "barcode": "string",
    "orderQty": 0,
    "scanQty": 0,
    "issueQty": 0,
    "cancelQty": 0,
    "price": 0,
    "totalPrice": 0,
    "length": 0,
    "width": 0,
    "height": 0,
    "weight": 0,
    "productName": "string",
    "uomName": "string",
    "zone": {
      ... Zone short response
    },
    "location": {
      ... Warehouse location short response
    },
    "product": {
      ... Product short response
    },
    "uom": {
      ... UOM short response
    }
  }
]
```

> Schema

name | type| required |
----- | ----- |----- |
id      	    | string($uuid)                                   | -    |
outboundId    | string($uuid)                                   | true |
warehouseId   | string($uuid)                                   | -    |
zoneId      	| string($uuid)                                   | -    |
locationId    | string($uuid)                                   | -    |
productId     | string($uuid)                                   | true |
uomId     	  | string($uuid)                                   | true |
grnNo     	  | string                                          | -    |
batchNo     	| string                                          | -    |
lotNo     	  | string                                          | -    |
palletNo      | string                                          | -    |
barcode     	| string                                          | -    |
orderQty      | integer($int32)                                 | true |
scanQty     	| integer($int32)                                 | -    |
issueQty      | integer($int32)                                 | -    |
cancelQty     | integer($int32)                                 | -    |
price     	  | number($double)                                 | true |
totalPrice    | number($double)                                 | -    |
length      	| number($double)                                 | -    |
width     	  | number($double)                                 | -    |
height      	| number($double)                                 | -    |
weight      	| number($double)                                 | -    |
productName   | string                                          | -    |
uomName     	| string                                          | -    |
zone      	  | [ZoneShortResponse](zone.md)                               | -    |
location      | [LocationShortResponse](warehouse-location.md)  | -    |
product     	| [ProductShortResponse](product.md)              | -    |
uom     	    | [UomShortResponse](uom.md)                      | -    |
