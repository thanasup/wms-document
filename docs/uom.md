# Unit Of Measure
[Back to readme](../README.md)
> **Note:**
> * rename `id` to `uomId` for external used

## UOM short infomation
* **GET:** `/uom/short-information`
* **Media type:** `application/json` Controls Accept header.
### Parameters `Query`
name | type | required 
----- | ----- | ----- |
SearchTxt  | string   | false |
Code       | string   | false |
Name       | string   | false |
### Responses
* **Code:** `200` _Success_
> Example Value
```json
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "code": "string",
    "name": "string",
    "symbol": "string",
    "noOfUnit": 0,
    "precision": 0
  }
]
```

> Schema

name | type |
----- | ----- |
id	       | string($uuid) |
code	     | string  |
name	     | string  |
symbol	   | string  |
noOfUnit	 | integer($int32) |
precision	 | integer($int32) |


--------------
